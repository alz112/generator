package main

// Please note this solution is based on the Two Fast Implementations of
// the "Minimal Standard" Random Number Generator paper by David G. Carta
// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.94.3416&rep=rep1&type=pdf

// Generator struct represents a random number generator
type Generator struct {
	seed       uint64
	multiplier uint64
	modulus    uint64
}

// NewGenerator creates a generator using given initial seed and multiplier
func NewGenerator(seed uint64, multiplier uint64) *Generator {
	g := &Generator{}
	g.seed = seed
	g.multiplier = multiplier
	g.modulus = 0x7FFFFFFF // 2^31-1 (Mersenne Prime)
	return g
}

func (g *Generator) next() uint64 {
	m := g.multiplier * g.seed
	p := m & g.modulus // take the 31 low bits
	q := m >> 31       // take the high bits
	sum := p + q

	// If sum is less than modulus we're fine
	if sum < g.modulus {
		g.seed = sum
		return g.seed
	}

	// If sum is higher we simply need to subtract the modulus once
	// as the sum can never be more than twice the modulus
	g.seed = (sum & g.modulus) + 1
	return g.seed
}

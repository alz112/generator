package main

import "testing"

func TestGenerator(t *testing.T) {
	g := NewGenerator(1, 16807)

	var random uint64
	for i := 0; i < 10000; i++ {
		random = g.next()
	}

	if random != 1043618065 {
		t.Error("10,000th invocation is not equal to 1043618065")
	}
}

func BenchmarkGenerator(b *testing.B) {
	g := NewGenerator(1, 16807)
	for i := 0; i < b.N; i++ {
		g.next()
	}
}

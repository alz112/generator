package main

import (
	"flag"
	"fmt"
	"time"
)

func main() {
	i := flag.Int("i", 40000000, "Number of iterations")
	p := flag.Bool("p", false, "Print will output generated values")
	s1 := flag.Int("s1", 65, "Generator 1 seed")
	s2 := flag.Int("s2", 8921, "Generator 2 seed")
	flag.Parse()

	// Multipliers are carefully chosen for modulus 2^31-1
	// 16807 is the lowest full-period-multiplier, 48271, 69621 are other alternatives
	// Full period multipliers ensure the generator includes every number from 1
	// up until one less than the modulus
	g1 := NewGenerator(uint64(*s1), 16807)
	g2 := NewGenerator(uint64(*s2), 48271)

	start := time.Now()
	pairs := 0

	iterations := *i
	for iter := 0; iter < iterations; iter++ {
		v1 := g1.next()
		v2 := g2.next()

		// Apply masks since we only require 16bit precision
		if v1&0xFFFF == v2&0xFFFF {
			pairs++
		}

		if *p == true {
			fmt.Printf("%d\t%d\n", v1, v2)
		}
	}

	elapsed := time.Since(start)
	fmt.Printf("Found %d matches in %s\n", pairs, elapsed)
}
